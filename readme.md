Criar base de dados de sua preferencia e configurar o arquivo application.properties
OBS: o projeto já esta configurado para Oracle 10G+, caso use outro banco de dados, lembre-se de importar a biblioteca do MAVEN

Estrutura de tabelas simples:
__________________________________________________________________________
create table voo(
codigo varchar(50) not null,
origem varchar(50) not null,
destino varchar(50) not null,
dt_saida date not null,
dt_Chegada date not null,
valor number(10,2) not null,
codigo_empresa int not null)

create table aeroportos(
nome varchar(100) not null,
codigo varchar(50) not null,
cidade varchar(50) not null)

create table empresa_voo(
codigo int not null,
nome varchar(50) not null)
    
insert into empresa_voo values(1, '99 Planes');
insert into empresa_voo values(2, 'URBES Air');
__________________________________________________________________________

Realize as importações do Maven e execute o projeto.
