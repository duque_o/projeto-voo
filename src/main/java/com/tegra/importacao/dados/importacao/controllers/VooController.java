package com.tegra.importacao.dados.importacao.controllers;

import com.tegra.importacao.dados.importacao.models.Voo;
import com.tegra.importacao.dados.importacao.service.VooService;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/voo")
public class VooController {

    private VooService vooService;

    @Autowired
    public VooController(VooService vooService) {
        this.vooService = vooService;
    }

    @ApiOperation(value = "Buscar os aeroportos")
    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<Voo>> buscarAtividades(
            @RequestParam("origem") String origem,
            @RequestParam("destino") String destino,
            @RequestParam("data")Date dt) {
        try {
            return ResponseEntity.ok().body(vooService.buscarVoo(origem, destino, dt));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
