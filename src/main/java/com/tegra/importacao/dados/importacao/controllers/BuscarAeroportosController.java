package com.tegra.importacao.dados.importacao.controllers;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import com.tegra.importacao.dados.importacao.service.AeroportoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/aeroporto")
public class BuscarAeroportosController {

    private AeroportoService aeroportoService;

    @Autowired
    public BuscarAeroportosController(AeroportoService aeroportoService) {
        this.aeroportoService = aeroportoService;
    }

    @ApiOperation(value = "Buscar os aeroportos")
    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<Aeroporto>> buscarAtividades() {
        return ResponseEntity.ok().body(aeroportoService.buscarAeroportos());
    }

}
