package com.tegra.importacao.dados.importacao.repository;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import com.tegra.importacao.dados.importacao.models.Voo;
import com.tegra.importacao.dados.importacao.models.VooPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface VooRepository extends JpaRepository<Voo, VooPK> {

    List<Voo> findAllByOrigemAndDestinoAndVoo_DataSaidaBetween(Aeroporto origem,
                                                               Aeroporto destino,
                                                               Date dtInicio,
                                                               Date dtFinal);
}
