package com.tegra.importacao.dados.importacao.service;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import com.tegra.importacao.dados.importacao.models.Voo;
import com.tegra.importacao.dados.importacao.repository.AeroportoRepository;
import com.tegra.importacao.dados.importacao.repository.VooRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class VooService {

    private VooRepository vooRepository;
    private AeroportoRepository aeroportoRepository;

    @Autowired
    public VooService(VooRepository vooRepository, AeroportoRepository aeroportoRepository) {
        this.vooRepository = vooRepository;
        this.aeroportoRepository = aeroportoRepository;
    }

    public List<Voo> buscarVoo(String origem, String destino, Date data) throws NotFoundException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.HOUR_OF_DAY, 12);
        Date dtFinal = cal.getTime();
        Optional<Aeroporto> aeroportoOrigem = aeroportoRepository.findById(origem);
        Optional<Aeroporto> aeroportoDesitno = aeroportoRepository.findById(destino);
        if(aeroportoDesitno.isPresent() && aeroportoOrigem.isPresent()){
            return vooRepository.findAllByOrigemAndDestinoAndVoo_DataSaidaBetween(aeroportoOrigem.get(), aeroportoDesitno.get(), data, dtFinal);
        }
        throw new NotFoundException("Não encontrado dados!");
    }
}
