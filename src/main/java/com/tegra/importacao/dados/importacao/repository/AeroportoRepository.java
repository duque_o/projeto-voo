package com.tegra.importacao.dados.importacao.repository;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AeroportoRepository extends JpaRepository<Aeroporto, String> {
}
