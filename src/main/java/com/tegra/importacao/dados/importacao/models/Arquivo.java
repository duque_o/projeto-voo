package com.tegra.importacao.dados.importacao.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "arquivo_voo")
public class Arquivo {

    @Id
    @Column(name = "codigo")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "codigo_empresa")
    private Empresa empresa;

    @Column(name = "dt_importacao")
    private Date dataImportacao;

}
