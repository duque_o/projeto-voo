package com.tegra.importacao.dados.importacao.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "empresa_voo")
public class Empresa {

    @Id
    @Column(name = "codigo")
    private Integer id;

    @Column(name = "nome")
    private String nomeEmpresa;
}
