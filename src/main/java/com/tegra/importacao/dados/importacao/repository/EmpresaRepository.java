package com.tegra.importacao.dados.importacao.repository;

import com.tegra.importacao.dados.importacao.models.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

    Empresa findByNomeEmpresa(String nomeEmpresa);
}
