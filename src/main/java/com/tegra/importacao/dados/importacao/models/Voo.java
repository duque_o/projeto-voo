package com.tegra.importacao.dados.importacao.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "voo")
public class Voo {

    @EmbeddedId
    private VooPK voo;

    @OneToOne
    @JoinColumn(name = "origem")
    private Aeroporto origem;

    @OneToOne
    @JoinColumn(name = "destino")
    private Aeroporto destino;

    @Column(name = "dt_chegada")
    private Date dataChegada;

    @Column(name = "valor")
    private Double valor;

    @OneToOne
    @JoinColumn(name = "codigo_empresa")
    private Empresa empresa;

}
