package com.tegra.importacao.dados.importacao.service;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import com.tegra.importacao.dados.importacao.repository.AeroportoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AeroportoService {

    private AeroportoRepository aeroportoRepository;

    @Autowired
    public AeroportoService(AeroportoRepository aeroportoRepository) {
        this.aeroportoRepository = aeroportoRepository;
    }

    public List<Aeroporto> buscarAeroportos(){
        return aeroportoRepository.findAll();
    }
}
