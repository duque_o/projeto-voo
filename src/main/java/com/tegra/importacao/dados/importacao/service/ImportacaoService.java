package com.tegra.importacao.dados.importacao.service;

import com.tegra.importacao.dados.importacao.models.Aeroporto;
import com.tegra.importacao.dados.importacao.models.Empresa;
import com.tegra.importacao.dados.importacao.models.Voo;
import com.tegra.importacao.dados.importacao.models.VooPK;
import com.tegra.importacao.dados.importacao.repository.AeroportoRepository;
import com.tegra.importacao.dados.importacao.repository.EmpresaRepository;
import com.tegra.importacao.dados.importacao.repository.VooRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jsonb.JsonbAutoConfiguration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class ImportacaoService {

    private static final Logger log = LoggerFactory.getLogger(ImportacaoService.class);

    private static final String NOME_99_PLANES = "99 Planes";
    private static final String NOME_URBES_AIR = "URBES Air";

    private static final String ARQUIVO_99_PLANES = "99planes.json";
    private static final String ARQUIVO_AEROPORTOS = "aeroportos.json";
    private static final String ARQUIVO_AIR_URBES = "uberair.csv";

    private EmpresaRepository empresaRepository;
    private VooRepository vooRepository;
    private AeroportoRepository aeroportoRepository;

    @Autowired
    public ImportacaoService(EmpresaRepository empresaRepository, VooRepository vooRepository, AeroportoRepository aeroportoRepository) {
        this.empresaRepository = empresaRepository;
        this.vooRepository = vooRepository;
        this.aeroportoRepository = aeroportoRepository;
    }

    private Date getDate(String dt, String hora){
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Date date = null;
        try {
            date = simpleDateFormat.parse(dt + " "+ hora);
        } catch (java.text.ParseException e) {
            log.error(e.getMessage());
        }
        return date;
    }

    private Date validarDataSaida(Date dtSaida, Date dtChegada){
        if(dtChegada.before(dtSaida)){
            Calendar cal = Calendar.getInstance();
            cal.setTime(dtChegada);
            cal.add(Calendar.DAY_OF_MONTH, 1);
            return cal.getTime();
        }
        return dtChegada;
    }

    private Double convertToDouble(JSONObject obj, String field){
        return obj.get(field) instanceof Double ? (Double) obj.get(field) : ((Long) obj.get(field)).doubleValue();
    }

    @Scheduled(fixedDelay = 5000)
    public void importarAeroportos(){
        JSONArray jsonArray;
        JSONParser jsonParser = new JSONParser();

        try {
            jsonArray = (JSONArray) jsonParser.parse(new FileReader(ARQUIVO_AEROPORTOS));
            Iterator i = jsonArray.iterator();

            while(i.hasNext()) {
                JSONObject jsonObject = (JSONObject) i.next();
                Aeroporto aeroporto = new Aeroporto();
                aeroporto.setCidade((String) jsonObject.get("cidade"));
                aeroporto.setCodigo((String) jsonObject.get("aeroporto"));
                aeroporto.setNome((String) jsonObject.get("nome"));
                aeroportoRepository.saveAndFlush(aeroporto);
            }
        } catch (IOException | ParseException e) {
            log.error(e.getMessage());
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void importarUrbesAir(){
        try {
            Empresa empresa = empresaRepository.findByNomeEmpresa(NOME_URBES_AIR);
            Files.lines(Paths.get(ARQUIVO_AIR_URBES)).skip(1).forEach(linha -> {
                String[] dados = linha.split(",");
                Optional<Aeroporto> aeroportoOrigem = aeroportoRepository.findById(dados[1]);
                Optional<Aeroporto> aeroportoDestino = aeroportoRepository.findById(dados[2]);
                if(aeroportoOrigem.isPresent() && aeroportoDestino.isPresent()) {

                    String dt = dados[3];
                    String hrChegada = dados[5];
                    String hrSaida = dados[4];
                    Date dateChegada = getDate(dt, hrChegada);
                    Date dateSaida = getDate(dt, hrSaida);
                    dateSaida = validarDataSaida(dateSaida, dateChegada);

                    Voo voo = new Voo();
                    voo.setEmpresa(empresa);
                    voo.setDestino(aeroportoDestino.get());
                    voo.setOrigem(aeroportoOrigem.get());
                    VooPK vooPK = new VooPK();
                    vooPK.setCodigo((String) dados[0]);
                    vooPK.setDataSaida(dateSaida);
                    voo.setVoo(vooPK);
                    voo.setDataChegada(dateChegada);
                    voo.setValor(Double.parseDouble(dados[6]));
                    vooRepository.saveAndFlush(voo);
                }else{
                    log.error("Aeroporto origem ou destino não cadastrado. Origem: "+ dados[1] +" Destino: "+dados[2]);
                }
            });
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void importar99Plains() {
        JSONArray jsonArray;
        JSONParser jsonParser = new JSONParser();

        Empresa empresa = empresaRepository.findByNomeEmpresa(NOME_99_PLANES);

        try {
            jsonArray = (JSONArray) jsonParser.parse(new FileReader(ARQUIVO_99_PLANES));
            Iterator i = jsonArray.iterator();

            while(i.hasNext()) {
                JSONObject jsonObject = (JSONObject) i.next();

                String dt = (String) jsonObject.get("data_saida");
                String hrChegada = (String) jsonObject.get("chegada");
                String hrSaida = (String) jsonObject.get("saida");
                Date dateChegada = getDate(dt, hrChegada);
                Date dateSaida = getDate(dt, hrSaida);
                dateSaida = validarDataSaida(dateSaida, dateChegada);

                Voo voo = new Voo();
                VooPK vooPK = new VooPK();
                Optional<Aeroporto> aeroportoOrigem = aeroportoRepository.findById((String) jsonObject.get("origem"));
                Optional<Aeroporto> aeroportoDestino = aeroportoRepository.findById((String) jsonObject.get("destino"));
                if(aeroportoOrigem.isPresent() && aeroportoDestino.isPresent()) {
                    vooPK.setCodigo((String) jsonObject.get("codigo"));
                    voo.setOrigem(aeroportoOrigem.get());
                    voo.setDestino(aeroportoDestino.get());
                    vooPK.setDataSaida(dateSaida);
                    voo.setDataChegada(dateChegada);
                    voo.setValor(convertToDouble(jsonObject, "valor"));
                    voo.setEmpresa(empresa);
                    voo.setVoo(vooPK);
                    vooRepository.saveAndFlush(voo);
                }else{
                    log.error("Aeroporto origem ou destino não cadastrado. Origem: "+ jsonObject.get("origem") +" Destino: "+jsonObject.get("destino"));
                }
            }
        } catch (ParseException | IOException e) {
            log.error(e.getMessage());
        }


    }
}
